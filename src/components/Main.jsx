import React from 'react';
import {Round, TripleRound, Square} from "../helpers/Shapes.js";
import {Waves} from "../helpers/Waves.js";
const CH = "*", 
	CH_E = "#",
	WIDTH = 300,
	HEIGHT = 300;

const customStyles = {
	"position": "relative", 
	"width": `${WIDTH}px`, 
	"height": `${HEIGHT}px`,
	"lineHeight": 1,
	"fontFamily": "monospace",
}
export default class Main extends React.Component{
		constructor(props){
			super(props);
			this.state = {
				lake: undefined, 
				value: 'square',
			};
		}
		
		componentDidMount(){
			const {
				oneSize
			} = this.refs;
			this.wave = new Waves(this.getSizes(oneSize));
		}

		getSizes = (oneSize) => ({
			elementWidth:oneSize.offsetWidth,
			elementHeight:oneSize.offsetHeight,
			height:HEIGHT,
			width:WIDTH,
			cb: this.setState.bind(this),
		});
		
		getEntity = (value) =>{
			switch (this.state.value){
				case "square": 
			 		return Square;
			 	case "wave":
			 		return TripleRound;
				default: 
					return Round;
			}
		};
		
		getClickPos = (e) =>({
			eW:e.clientX - e.currentTarget.offsetLeft,
			eH:e.clientY - e.currentTarget.offsetTop,
		});

		lakeClickHandle(e){
			const pos = this.getClickPos(e),
				entity = this.getEntity(this.state.value);
			this.wave.add(pos.eW, pos.eH, entity);
		}

		renderLake (lake){
			return lake.map((row, i)=>{
				return (
					<span key={i}>
						{row.join("")}
					<br />
					</span>
				);
			});				
		}

		handleSelectChange(e){
			const {value} = e.target;
			this.setState({value});
		}

		render(){
			const 
				{ lake } = this.state,
				self = this,
				lakeProps = {
					style: customStyles,
					onClick: this.lakeClickHandle.bind(self),
				};

			return (
				<div>
					<div {...lakeProps}>
						<span style={{position: "absolute"}} ref="oneSize">*</span>
						{lake && 
							<div className="b-lake">
								{this.renderLake(lake)}				
							</div>
						}
					</div>
					<div style={{"padding":"20px"}}>
					<select onChange={this.handleSelectChange.bind(this)}>
						<option value="square">Square</option>
						<option value="circle">Circle</option>
						<option value="wave">Wave</option>
					</select>
					</div>
				</div>
			
			
		);
	}
}





