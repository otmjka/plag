const CH = "*", 
	CH_E = "#";

export function Waves(o){
	const self=this;
	this.list = [];
	this.cb = o.cb;
	this.elementWidth = o.elementWidth;
	this.elementHeight = o.elementHeight;
	this.lakeWidth = o.width / o.elementWidth;
	this.lakeHeight = o.height / o.elementHeight;
	this.cb({lake: getLake(this.lakeWidth, this.lakeHeight, CH)});
	return this;
}

Waves.prototype.add = function add(eW, eH, element){
	let xCenter = Math.ceil(eW/this.elementWidth),
	yCenter = Math.ceil(eH/this.elementHeight);
	
	const el = new element(xCenter, yCenter, this.lakeWidth, this.lakeHeight);
	
	this.list.push(el);
	clearTimeout(this.timerId);
	this.timerId = void 0;
	this.render();
}

Waves.prototype.render = function (){
	const {list} = this;

	const newList = clearList(list);
	let lake = void 0;
	let fn = void 0;
	if (newList.length > 0) {
		fn = check(list);
		this.timerId = setTimeout(this.render.bind(this), 200)
		lake = getLake( this.lakeWidth, this.lakeHeight, CH, fn );
		this.cb({lake});
	} else {
		lake = getLake( this.lakeWidth, this.lakeHeight, CH, fn );
		this.cb({lake});
	}
	
	
	
	this.list = newList;

	//helpers
	
	function clearList(list){
		let i = 0;
		const l = list.length;
		while (i < l){
			if (list[i] == null) { i++; continue};
			const b = list[i].isOver( list[i].R + 1 );
			if (b === true){
				list[i] = void 0;
			} else {
				list[i].R = list[i].R + 1;
			}
			i++;
		}
		return list.filter((item)=> (item != null));
	}

	function check(list){
		return (x, y) => {
				let i = 0, result = void 0;
				while (i < list.length){
					if (list[i] == null) { i++; continue};
					if (result !== "#"){
						result = list[i].render(x, y);
						
					}
					i++;
				}
				return result;
			}
	}
}

function getLake(xSize, ySize, character, fn){
	let iY = 0;
	let lake = [];
	while (iY<ySize){
		lake.push(getLine({xSize, character, y: iY, fn}))
		iY++;
	}
	return lake;
}

function getLine(o = {xSize:0, character: "*", y: 0, fn: void 0}){
	const {xSize, character:ch, fn} = o;
	let iX = 0,
	result = [];
	while (iX<xSize){
		if (fn == null){
			result.push(ch);
		} else
			result.push(fn(iX, o.y));
		iX++;
	}
	return result;
}
