const CH = "*", 
	CH_E = "#";

export function Shape(xCenter, yCenter, width, height){
	this.xCenter = xCenter;
	this.yCenter = yCenter;
	this.width = width;
	this.height = height;
	this.R = 0;
	this.BoundX = Math.max(width-xCenter, width),
	this.BoundY = Math.max(height-yCenter, height);
}

export function Round(){
	Shape.apply(this, arguments);
	const self = this;
	this.isOver = (next)=>((next > this.BoundX) && (next > this.BoundY));
	this.render = (x, y)=>
			(RoundCheck(self.xCenter, self.yCenter, CH_E, CH, self.R)(x, y));
}
export function TripleRound(){
	Shape.apply(this, arguments);
	const self = this;
	this.isOver = (next)=>((next - 2 > this.BoundX) && (next - 2 > this.BoundY));
	this.render = (x, y)=>{
			let result = RoundCheck(self.xCenter, self.yCenter, CH_E, CH, self.R)(x, y);
			result = RoundCheck(self.xCenter, self.yCenter, CH_E, CH, self.R - 5)(x, y);
			return result;
	}
}
export function Square(){
	Shape.apply(this, arguments);
	const self = this;
	this.isOver = (next)=>((next > this.BoundX) && (next > this.BoundY));
	
	this.render = (x, y)=>
			(SquareCheck(self.xCenter, self.yCenter, CH_E, CH, self.R)(x, y));
}

function RoundCheck(xSize, ySize, fCh, eCh, R){ 
	return (x, y) =>{
		const X = (x - xSize),
		Y = (y - ySize);
		let res = Math.ceil(R*R - (X*X + Y*Y));
		
		return (Math.abs(res) < 5 ) 
			? fCh
			: eCh;
	}
}
function SquareCheck(xCenter, yCenter, fCh, eCh, R){ 
	return (x, y) =>{
		const res = (((x === (xCenter - R)) || (x === (xCenter + R))) && 
				((y >= (yCenter - R)) && (y <= (yCenter + R)))) 
		
			|| (((y === (yCenter - R)) || (y === (yCenter + R))) && 
				((x >= (xCenter - R)) && (x <= (xCenter + R))));
		return (res) 
			? fCh
			: eCh;
	}
}